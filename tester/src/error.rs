use thiserror::Error;

#[derive(Debug, Error)]
pub enum TesterError {
    #[error("reqwest error")]
    RequestError(#[from] reqwest::Error),
}

pub type Result<T> = std::result::Result<T, TesterError>;
