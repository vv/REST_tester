#[derive(Clone, Copy)]
pub enum RequestType {
    Get,
    GetAll,
    Post,
    Put,
    Delete,
}

pub struct Response {
    pub url: String,
    pub request_type: RequestType,
    pub status: reqwest::StatusCode,
}
