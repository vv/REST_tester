use reqwest::Client;
use serde::Serialize;

use crate::{
    error::Result,
    types::{RequestType, Response},
};

pub struct TestClient<T: Serialize> {
    single_url: String,
    multiple_url: String,
    request_types: Vec<RequestType>,
    post_body: T,
    put_body: T,
    id: Option<i32>,
}

impl<T: Serialize> TestClient<T> {
    pub fn new(
        single_url: String,
        request_types: Vec<RequestType>,
        post_body: T,
        put_body: T,
    ) -> TestClient<T> {
        let mut multiple_url = single_url.clone();
        multiple_url.push('s');

        TestClient {
            single_url,
            multiple_url,
            request_types,
            post_body,
            put_body,
            id: None,
        }
    }

    pub fn with_id(
        single_url: String,
        request_types: Vec<RequestType>,
        post_body: T,
        put_body: T,
        id: i32,
    ) -> TestClient<T> {
        let mut client = TestClient::new(single_url, request_types, post_body, put_body);
        client.id = Some(id);

        client
    }

    pub async fn run_test(self, client: Client) -> Result<()> {
        for type_ in self.request_types.iter() {
            self.run_one_test(client.clone(), type_.clone()).await;
        }

        Ok(())
    }

    async fn run_one_test(&self, client: Client, type_: RequestType) -> Result<Response> {
        let request = match type_ {
            RequestType::GetAll => client.get(&self.multiple_url),
            RequestType::Post => client.post(&self.multiple_url).json(&self.post_body),
            RequestType::Get => client.get(&self.single_url_with_id()),
            RequestType::Put => client.put(&self.single_url_with_id()).json(&self.put_body),
            RequestType::Delete => client.delete(&self.single_url_with_id()),
        };

        let res = request.send().await?;

        Ok(Response {
            url: res.url().to_string(),
            request_type: type_,
            status: res.status(),
        })
    }

    fn single_url_with_id(&self) -> String {
        format!(
            "{}/{}",
            self.single_url,
            self.id
                .expect("run post method first or set the id with another constructor")
        )
    }
}
